
public class Main {
	
	public static void main(String args[]){
		try{
			wakeUp();
		
			getReadyForSchool();
		
			goToClass();
		
			eatLunch();
		
			goToMoreClass();
		
			goHome();
		}catch(AbductionException e)
		{
			System.out.println("Justin's teach was abducted by an alien. Hurray!");
			System.out.println("Justin goes back to sleep");
		}
		
	}

	private static void goHome() {
		System.out.println("Justin takes his hoverboard home. (Don't take that on an airplane).");
		// TODO Auto-generated method stub
		
	}

	private static void goToMoreClass() {
		System.out.println("Justin tries to stay awayke in his later classes.");
		// TODO Auto-generated method stub
		
	}

	private static void eatLunch() {
		System.out.println("Justin eats lunch. He has a...McRib 'sandwich'");
		// TODO Auto-generated method stub
		
	}

	private static void goToClass() {
		System.out.println("Justin can't hear anything because of the fan.");
		// TODO Auto-generated method stub
		
	}

	private static void getReadyForSchool() {
		System.out.println("Justin gets ready for school.");
		// TODO Auto-generated method stub
		
	}

	private static void wakeUp() throws AbductionException {
		System.out.println("Justin wakes up.");
		
		
		// Here's how to throw an exception
		
		//Option 1: just ignore the exception and let the program terminate.
		//throw new AbductionException();
		
		//Option 2: catch the exception
		
		//Option 3: Just keep throwing the exception
		try{
			
			throw new AbductionException();
		}catch(AbductionException e)
		{
			System.out.println("Justin's teach was abducted by an alien. Hurray!");
			System.out.println("Justin goes back to sleep");
		}
		
	}

}
