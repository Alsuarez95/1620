/**
 * Creating our own exception class.
 * 
 * 2 types of exceptions:
 * checked - All checked exceptions have to be caught. Extends exception.
 * unchecked - don't have to be caught. Extends RuntimeException.
 * 
 * @author Gualberto
 *
 */
public class AbductionException extends RuntimeException{
	

}
